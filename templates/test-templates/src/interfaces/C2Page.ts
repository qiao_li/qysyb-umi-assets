export default interface C2Page<T> {
    contents: T[],
    pageIndex: number,
    pageSize: number,
    total: number,
    totalPage: number
}