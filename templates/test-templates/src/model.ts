import * as entityService from './service';
import C2Page from './interfaces/C2Page';
import ENTITY from "../@/entities/ENTITY";

export default {
    namespace: 'BLOCK_NAME',
    state: {
        contents: [],
        total: null,
        pageIndex: null,
    },
    reducers: {
        save(state, { payload: { contents, total, pageIndex } }) {
            return { ...state, contents, total, pageIndex };
        },
    },
    effects: {
        *fetch({ payload: { pageIndex = 1, pageSize } }, { call, put }) {
            const { contents, total }: C2Page<ENTITY> = yield call(entityService.fetch, { pageIndex, pageSize });
            yield put({
                type: 'save',
                payload: {
                    contents,
                    total,
                    pageIndex,
                },
            });
        },
        *remove({ payload: id }, { call, put, select }) {
            yield call(entityService.remove, id);
            const page = yield select(state => state['BLOCK_NAME'].page);
            yield put({ type: 'fetch', payload: { page } });
        },
        *patch({ payload: { id, values } }, { call, put, select }) {
            yield call(entityService.patch, id, values);
            const page = yield select(state => state['BLOCK_NAME'].page);
            yield put({ type: 'fetch', payload: { page } });
        },
        *create({ payload: values }, { call, put, select }) {
            yield call(entityService.create, values);
            const page = yield select(state => state['BLOCK_NAME'].page);
            yield put({ type: 'fetch', payload: { page } });
        },
    },
};
