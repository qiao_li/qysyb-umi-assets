import datas from "./data"
const PAGE_SIZE = 3;

let data = datas
function uid(len) {
  len = len || 7;
  return Math.random()
    .toString(35)
    .substr(2, len);
}

function getData(pageIndex, pageSize) {
  const start = (pageIndex - 1) * (pageSize || PAGE_SIZE);
  console.log(data)
  return {
    status: 'success',
    total: data.length,
    pageSize,
    pageIndex,
    contents: data.slice(start, start + (pageSize || PAGE_SIZE)),
  };
}

export default {
  'GET /proxy/BLOCK_NAME': (req, res) => {
    res.json(getData(parseInt(req.query.page) || 1, parseInt(req.query.rows)));
  },
  'DELETE /proxy/BLOCK_NAME/:id': (req, res) => {
    data = data.filter(item => `${item.id}` !== `${req.params.id}`);
    res.end('ok');
  },
  'PATCH /proxy/BLOCK_NAME/:id': (req, res) => {
    data.forEach(item => {
      if (`${item.id}` === `${req.params.id}`) {
        Object.assign(item, req.body);
      }
    });
    res.end('ok');
  },
  'POST /proxy/BLOCK_NAME': (req, res) => {
    data.push({
      ...req.body,
      id: uid(10),
    });
    res.end('ok');
  },
};
