import { Button, Divider, Dropdown, Icon, Menu } from 'antd';
import React from 'react';
import ProTable, { ProColumns, ActionType } from '@ant-design/pro-table';
import UmiUIFlag from '@umijs/ui-flag';
import { connect } from 'dva';
import { TableListParams } from '../typing.d';
import ENTITY from "../../@/entities/ENTITY";

class Table extends React.Component<{
    dispatch,
    contents,
    loading,
    total,
    pageIndex
}> {
    // const [sorter, setSorter] = useState({});
    ref
    constructor(props) {
        super(props)
        this.ref = React.createRef();
        this.state = {

        }
    }

    columns: ProColumns<ENTITY>[] = [
        {
            title: '事项编码',
            dataIndex: 'moduleNo',
        },
        {
            title: '事项名称',
            dataIndex: 'moduleName',
        },
        {
            title: '表单名称',
            dataIndex: 'formId.formName',
            sorter: true,
            align: 'right',
            renderText: (val: string) => `[ ${val} 自定义]`,
        },
        {
            title: '状态',
            dataIndex: 'status',
            valueEnum: {
                0: { text: '关闭', status: 'Default' },
                1: { text: '运行中', status: 'Processing' },
                2: { text: '已上线', status: 'Success' },
                3: { text: '异常', status: 'Error' },
            },
        },
        {
            title: '操作',
            dataIndex: 'option',
            valueType: 'option',
            render: () => (
                <>
                    <a>配置</a>
                    <Divider type="vertical" />
                    <a>订阅警报</a>
                </>
            ),
        },
    ];

    componentDidMount() {
        this.props.dispatch({
            type: 'BLOCK_NAME/fetch',
            payload: { pageIndex: 1, pageSize: 10 }
        });
    }

    onChange = (pageIndex, pageSize) => {
        this.props.dispatch({
            type: 'BLOCK_NAME/fetch',
            payload: { pageIndex, pageSize }
        });
    }

    onShowSizeChange = (pageIndex, pageSize) => {
        this.props.dispatch({
            type: 'BLOCK_NAME/fetch',
            payload: { pageIndex, pageSize }
        });
    }

    render() {
        return (
            <ProTable<ENTITY>
                headerTitle="查询表格"
                actionRef={this.ref}
                rowKey="key"
                // onChange={(_, _filter, _sorter) => {
                //   setSorter(`${_sorter.field}_${_sorter.order}`);
                // }}
                // params={{
                //     sorter,
                // }}
                toolBarRender={(action, { selectedRows }) => [
                    <>
                        <UmiUIFlag />
                    </>,
                    <Button icon="plus" type="primary">新建</Button>,
                    selectedRows && selectedRows.length > 0 && (
                        <Dropdown
                            overlay={
                                <Menu
                                    onClick={async e => {
                                        if (e.key === 'remove') {
                                            console.log('remove');
                                            action.reload();
                                        }
                                    }}
                                    selectedKeys={[]}
                                >
                                    <Menu.Item key="remove">批量删除</Menu.Item>
                                    <Menu.Item key="approval">批量审批</Menu.Item>
                                </Menu>
                            }
                        >
                            <Button>
                                批量操作 <Icon type="down" />
                            </Button>
                        </Dropdown>
                    ),
                ]}
                // tableAlertRender={(selectedRowKeys, selectedRows) => (
                //   <div>
                //     已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
                //     <span>
                //       服务调用次数总计 {selectedRows.reduce((pre, item) => pre + item.callNo, 0)} 万
                //     </span>
                //   </div>
                // )}
                // request={params => queryRule(params, dispatch)}
                dataSource={this.props.contents}
                pagination={{
                    onChange: this.onChange,
                    total: this.props.total,
                    showSizeChanger: true,
                    defaultPageSize: 10,
                    onShowSizeChange: this.onShowSizeChange
                }}
                columns={this.columns}
            />
        )
    }
};
function mapStateToProps(state) {
    const { contents, total, pageIndex } = state['BLOCK_NAME'];
    return {
        contents,
        total,
        pageIndex,
        loading: state.loading.models['BLOCK_NAME'],
    };
}

export default connect(mapStateToProps)(Table);