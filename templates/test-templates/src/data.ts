const datas = [
    {
        "appId": "speJ6FPaQlGYmDBahKDUcw",
        "formId": {
            "dbTableName": "TD_OA_TRAINING_EDUCATION",
            "formName": "自费教育",
            "formNo": "educationApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "4wHIQ1OJRAO7tPxidO_Avg",
            "operateBean": "EducationApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "Qo8RYnjUQy2tRVMPIobnaw",
        "moduleName": "自费教育",
        "moduleNo": "education",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "kxH6l9bvRcaIwCtpaahICA",
        "formId": {
            "dbTableName": "TD_ASSETS_ITMAINTAIN",
            "formName": "资产维修申请",
            "formNo": "assetMaintainAdd",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "CmK0-qsoSQuxEPLA0OEd1w",
            "operateBean": "AssetMaintainOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "O7WgrjsDR86MqH4ZMZVDYg",
        "moduleName": "资产维修申请",
        "moduleNo": "assetMaintainAdd",
        "moduleTypeId": "notice"
    },
    {
        "appId": "Y6GVieevScWw-k2MzjB3Vg",
        "formId": {
            "dbTableName": "111",
            "formName": "论文投稿申报",
            "formNo": "lwtgsb",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Fy3p94wcSfepOZ3f4mLmvQ",
            "operateBean": "PaperInfoService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.155:1022"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "MSn47pTeQa-kdY7XRDREMQ",
        "moduleName": "论文投稿申报",
        "moduleNo": "lwtgsb",
        "moduleTypeId": "newsInterview"
    },
    {
        "formId": {
            "dbTableName": "fgfhjkl",
            "formName": "年度报告",
            "formNo": "CreateAnnualReport",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "5Cd-NDL1S2a7MXtmhSQ0Dg",
            "operateBean": "AnnualReportService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "KJwoT4nOTaaPLIoXUjcL_g",
        "moduleName": "年度报告",
        "moduleNo": "CreateAnnualReport",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_JSJH_APPLY",
            "formName": "劳动竞赛计划申报",
            "formNo": "competitionPlan",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "1TEVZTrCT0mSPQyqcdWE_w",
            "operateBean": "CompetitionPlanOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "PtUY_CEBTha-K2nniVdgEg",
        "moduleName": "劳动竞赛计划申报",
        "moduleNo": "competitionPlan",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "1KKyQceHT6uFCdF462Ltiw",
        "formId": {
            "dbTableName": "TD_SECURITY_PASSCAR",
            "formName": "相关车辆出入证制证审批表",
            "formNo": "passcar",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "UvTd8n3LSEK2RpXNDXbn3g",
            "operateBean": "PasscarOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "nd_dzXh1Q0ejfBVnMFv6BA",
        "moduleName": "相关车辆出入证制证",
        "moduleNo": "passcar",
        "moduleTypeId": "notice",
        "unionCode": "XGCLCR"
    },
    {
        "appId": "Fks1ZZm5RU6dxyeHpztBqQ",
        "formId": {
            "dbTableName": "TD_DZJW_DHGL_APPLY",
            "formName": "党徽申领管理",
            "formNo": "dhglApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "lTIxeUqnSzGpljfOxr-bbQ",
            "operateBean": "DhglApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.200:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "lvgZEdRaRM-VTVVcRsditA",
        "moduleName": "党徽申领管理",
        "moduleNo": "dhglApply",
        "moduleTypeId": "newsInterview",
        "unionCode": "dhglApply"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_TWGZ_XFJ",
            "formName": "青春先锋奖申请",
            "formNo": "youthAward",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "nWAYzQBfR_q2_bwngKJsLQ",
            "operateBean": "YouthAwardOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "AgjrKjE2Qaut1KAyoWdcEw",
        "moduleName": "青春先锋奖申请",
        "moduleNo": "youthAward",
        "moduleTypeId": "notice",
        "unionCode": "youthAward"
    },
    {
        "appId": "speJ6FPaQlGYmDBahKDUcw",
        "formId": {
            "dbTableName": "TD_OA_TRAINING",
            "formName": "内部培训申请",
            "formNo": "internalTraining",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "QDKsB9BLRPC_HY8adJOtLA",
            "operateBean": "InternalTrainingOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "irS1sIZlSTG4QYPhvRiCEg",
        "moduleName": "内部培训",
        "moduleNo": "internalTraining",
        "moduleTypeId": "notice"
    },
    {
        "appId": "kxH6l9bvRcaIwCtpaahICA",
        "formId": {
            "dbTableName": "TD_ASSETS_ITCOMMUNICATION",
            "formName": "通讯流程申请",
            "formNo": "communicationApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "FLG5ktOQTYO4V69WrODPSQ",
            "operateBean": "CommunicationOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "PK8h7eraRlevEqNi_3nrDA",
        "moduleName": "通讯维护申请",
        "moduleNo": "communicationApply",
        "moduleTypeId": "notice"
    },
    {
        "appId": "kxH6l9bvRcaIwCtpaahICA",
        "formId": {
            "dbTableName": "TD_ASSETS_ITRECEIVE_APPLY",
            "formName": "IT资产领用申请",
            "formNo": "assetRApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "N1eRum5lRMCUgsrN7Au-PQ",
            "operateBean": "AssetRApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "y0SfGTDUREizauMijY2IoQ",
        "moduleName": "IT资产领用申请",
        "moduleNo": "assetRApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "formId": {
            "dbTableName": "111",
            "formName": "技术服务合同登记",
            "formNo": "contract/createTechnicalContract",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "_f0AJB-IRpSE4SSelIqHNw",
            "operateBean": "TechnicalContractService",
            "operateType": 2,
            "remoteUrl": "rmi://10.254.9.163:1024"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "ZgV4Dh7YTQ26kmgJwPbgBA",
        "moduleName": "技术服务合同登记",
        "moduleNo": "technicalContract",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "Y6GVieevScWw-k2MzjB3Vg",
        "formId": {
            "dbTableName": "CTQTC_PROJECT_APPROVAL",
            "formName": "项目立项表单",
            "formNo": "CreateProjectManagement",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "tPogIbAYR1eMHNwHuQOEgA",
            "operateBean": "ProjectManagementService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "CvEsDDMQTu6CSQKHqXSQlA",
        "moduleName": "项目立项",
        "moduleNo": "CreateProjectManagement",
        "moduleTypeId": "newsInterview"
    },
    {
        "formId": {
            "dbTableName": "CTQTC_PROJECT_CONTRACT",
            "formName": "合同登记",
            "formNo": "CreateProjectContract",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "6Bp0whfGTPqHJUktT7L_7w",
            "operateBean": "ProjectContractService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "8FVOIjcuR_yn3AxrpyVo9Q",
        "moduleName": "合同登记",
        "moduleNo": "CreateProjectContract",
        "moduleTypeId": "notice"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_JLAX_APPLY",
            "formName": "敬老爱心申请",
            "formNo": "respectOldApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "3aZUSOaDT2Oe0bSdF2pYoQ",
            "operateBean": "RespectOldApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "bxO0iHRsRrWxTfJpe1IaiQ",
        "moduleName": "敬老爱心申请",
        "moduleNo": "respectOldApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_OPEN_MENU_APPLY",
            "formName": "办事公开目录",
            "formNo": "openMenuApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "UMzFlPGiRiePPa4vOA-z8g",
            "operateBean": "OpenMenuApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "dVooI3MUSsum57tkz8JJHQ",
        "moduleName": "办事公开目录",
        "moduleNo": "openMenu",
        "moduleTypeId": "newsInterview",
        "unionCode": "openMenu"
    },
    {
        "formId": {
            "dbTableName": "TD_ENGINEERING_REPBNSAPPLY_BNS",
            "formName": "零星维修项目业务审批",
            "formNo": "repbnsapplyBns",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "WvTeNwmLQuOZbIgPIHOVIQ",
            "operateBean": "RepbnsapplyBnsOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.159:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "tvLCyz1iSDu-5igqJNlQDA",
        "moduleName": "零星维修项目业务审批",
        "moduleNo": "RepbnsapplyBns",
        "moduleTypeId": "notice"
    },
    {
        "formId": {
            "dbTableName": "ctqtc_project_cancel",
            "formName": "项目销号表单",
            "formNo": "CreateProjectCancel",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "ZP3xsyQORpexkExIsu6oIg",
            "operateBean": "ProjectCancelService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "1CM8m_VVQIie7MUgBSWlSA",
        "moduleName": "项目销号",
        "moduleNo": "CreateProjectCancel",
        "moduleTypeId": "notice",
        "unionCode": "CreateProjectCancel"
    },
    {
        "appId": "34u0xOFVRFSAx_ZyrmlRGQ",
        "formId": {
            "dbTableName": "TD_INTEGRATEDT_USEENERGY_INFO",
            "formName": "用能申请",
            "formNo": "UseEnergy",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Imo4qdYSQO6joOA8ormoQQ",
            "operateBean": "UseEnergyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.196:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "MF8lRxrNQ6mY33-mqBx9zg",
        "moduleName": "用能申请",
        "moduleNo": "UseEnergy",
        "moduleTypeId": "newsInterview",
        "unionCode": "UserEnergy"
    },
    {
        "appId": "Fks1ZZm5RU6dxyeHpztBqQ",
        "formId": {
            "dbTableName": "TD_DZJW_DFTZ_APPLY",
            "formName": "个人党费调整申请",
            "formNo": "dftzApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "IZYfta3hSdO63qMPlt61qg",
            "operateBean": "DftzApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.200:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "4PQ-Lz6QQo2y53BacirJ8A",
        "moduleName": "个人党费调整申请",
        "moduleNo": "dftzApply",
        "moduleTypeId": "newsInterview",
        "unionCode": "dftzApply"
    },
    {
        "formId": {
            "dbTableName": "TD_ASSETS_LOW_RECEIVE",
            "formName": "低值易耗品领用申请",
            "formNo": "assetsLowReceiveApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "OKJRx_PxQ1CzYyCQps93sQ",
            "operateBean": "AssetsLowReceiveOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "HO-fDYE7RdehDsBkeAXfLw",
        "moduleName": "低值易耗品领用流程",
        "moduleNo": "assetsLowReceiveApply",
        "moduleTypeId": "notice",
        "unionCode": "assetsLowReceiveApply"
    },
    {
        "formId": {
            "dbTableName": "TD_INTEGRATEDT_ZHGLKH_APPLY",
            "formName": "综合管理考核申请",
            "formNo": "integratedtAssessmentApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "fKkes2TSSOe7JLGDcIWNwQ",
            "operateBean": "IntegratedtAssessmentOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "jEbFzRX0RwmmTkhRqXDj-A",
        "moduleName": "综合管理考核申请",
        "moduleNo": "integratedtAssessmentApply",
        "moduleTypeId": "notice",
        "unionCode": "integratedtAssessmentApply"
    },
    {
        "formId": {
            "dbTableName": "TD_INTEGRATEDT_LSJLXM_APPLY",
            "formName": "内部临时奖励项目申报",
            "formNo": "temporaryRewardsApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "yJDqRG9MR3ORDNnFv9zlvA",
            "operateBean": "TemporaryRewardsOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.186:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "DdMZgLX8QNG2BaSOPoSzbQ",
        "moduleName": "内部临时奖励项目申报",
        "moduleNo": "temporaryRewardsApply",
        "moduleTypeId": "notice",
        "unionCode": "temporaryRewardsApply"
    },
    {
        "appId": "lyie5DzsQD2Pft6ct407sA",
        "formId": {
            "dbTableName": "TD_OA_NEWS_INTERVIEW",
            "formName": "新闻采访",
            "formNo": "NewsInterview",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "p84Uw0EOS5-apUS43YMQ3Q",
            "operateBean": "NewsInterviewOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "B8tG_Tr3S7i8sck429zHPg",
        "moduleName": "新闻采访",
        "moduleNo": "newsInterview",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "1T_MAYr8QEOjEgdjFFD--Q",
        "formId": {
            "dbTableName": "TEST_DB",
            "formName": "测试表单",
            "formNo": "testForm",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "LkbyyPj3R8Citgfz_TxABg",
            "operateBean": "EmptyFormOperator",
            "operateType": 2,
            "remoteUrl": "rmi://10.159.139.102:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "nyLvOQ2RQe26PiHCe5JeAg",
        "moduleName": "测试事项",
        "moduleNo": "testModule",
        "moduleTypeId": "notice"
    },
    {
        "appId": "QHX8tTgYQeeLLLRw-NP06g",
        "formId": {
            "dbTableName": "TD_ARCHIVE_BORROW",
            "formName": "档案借阅申请",
            "formNo": "archiveslibrary",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "XEFC13qjT3yczR4cqje_YQ",
            "operateBean": "archivesOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "5vip1PU2T-iNOCFjOI8NDg",
        "moduleName": "档案借阅申请",
        "moduleNo": "archiveslibrary",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "ldW5XGoiRkeeh4qrxQFcaw",
        "formId": {
            "dbTableName": "TD_OA_NEWS_INTERVIEW",
            "formName": "新闻采访测试表单",
            "formNo": "NewsInterviewTest",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "ne6LGXezQBqO4mPyV9WlAQ",
            "operateBean": "NewsInterviewOperate",
            "operateType": 2,
            "remoteUrl": "rmi://10.159.139.101:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "jsNTsaR9QpSVYtb3TTu_ig",
        "moduleName": "新闻采访测试",
        "moduleNo": "newsInterviewTest",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "Fks1ZZm5RU6dxyeHpztBqQ",
        "formId": {
            "dbTableName": "TD_HQFW_REPAST_ADJUST",
            "formName": "就餐调整",
            "formNo": "repastAdjust",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "D7a5iDxdTrCGxWZQw4Q2SQ",
            "operateBean": "RepastAdjustOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.200:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "eAUgQYmUQ2eJ-yiG7Zg-uA",
        "moduleName": "就餐调整",
        "moduleNo": "repastAdjust",
        "moduleTypeId": "notice"
    },
    {
        "appId": "MBSjCwkJTxirWOjHSLj7DA",
        "formId": {
            "dbTableName": "TD_OA_SCMGT_CONSTRUCT_MANAGE",
            "formName": "生产区施工管理",
            "formNo": "constructManageApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "PFIoghzuRWGMl_4MZM4QNA",
            "operateBean": "ConstructManageOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.200:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "vgZPjb9_SQ-M7-TkSxeDqA",
        "moduleName": "生产区施工管理",
        "moduleNo": "SCMGT",
        "moduleTypeId": "notice"
    },
    {
        "appId": "znb2M7TSQsqbFWV8ALeA4w",
        "formId": {
            "dbTableName": "TD_TRADE_GHYAGL_GHYA",
            "formName": "工会议案申请",
            "formNo": "TradeBillApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "8aNp4vcwRvaPgJnv_O_n9Q",
            "operateBean": "TradeBillOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "b_z9zNmOTFmOORikwlH70w",
        "moduleName": "工会议案申请",
        "moduleNo": "tradeBillFlow",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "qMKnOevAQDq5H8x1f-RvPQ",
        "formId": {
            "dbTableName": "TD_ARCHIVE_MATERTRAN",
            "formName": "非人事档案转递申请",
            "formNo": "archivesmatertran",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "cYRyGrJlQRKJcnqSleolaA",
            "operateBean": "archivesmatertranOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "HSAL6MjURnOmoCqygrr7BQ",
        "moduleName": "非人事档案转递申请",
        "moduleNo": "archivesmatertran",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "znb2M7TSQsqbFWV8ALeA4w",
        "formId": {
            "dbTableName": "TD_TRADE_GHBZGL_GHBZ",
            "formName": "工会报账申报",
            "formNo": "tradeReimbursement",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "jDlotp58SxemrrkT2FuhTA",
            "operateBean": "TradeReimbursementOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "gJSAbEc9SRG6ZTkjoHfrmw",
        "moduleName": "工会报账申报",
        "moduleNo": "tradeReimbursement",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "nDhEwlMlSVyXtH1nV3_EXA",
        "formId": {
            "dbTableName": "TD_HQFW_WATER_USAGE",
            "formName": "公用纯净水领用申报",
            "formNo": "waterUsage",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "hyzUQS4dTtmRepwGVtsHpw",
            "operateBean": "WaterUsageOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "RZOpW_sfTMuvu9YKAqQuCA",
        "moduleName": "公用纯净水领用申报",
        "moduleNo": "waterUsage",
        "moduleTypeId": "notice"
    },
    {
        "appId": "o1kmdj1QT4WYlqT3n-YhvQ",
        "formId": {
            "dbTableName": "TD_OA_OFFICE_COMPANY_DISPATCH",
            "formName": "厂级发文登记",
            "formNo": "CompanyDispatchRegister",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "qWr58UUETCymhDX297smaA",
            "operateBean": "CompanyDispatchOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.18.0.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "pFeRE1A0Raay4Jkk2r04AQ",
        "moduleName": "厂级发文",
        "moduleNo": "CompanyDispatch",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "8i5-b6fRTcGHrpRjB5AQKw",
        "formId": {
            "dbTableName": "TD_DZJW_SWXX_APPLY",
            "formName": "涉稳信息与思想动态申报",
            "formNo": "stabilityIdeaApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "au_1sqYaQzCU_UdWB0Nj9w",
            "operateBean": "StabilityInfoOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "Mv1t6aonQcm3H8KcNKjAIw",
        "moduleName": "涉稳信息与思想动态申报",
        "moduleNo": "stabilityIdeaApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "qMKnOevAQDq5H8x1f-RvPQ",
        "formId": {
            "dbTableName": "TD_ARCHIVE_WORKERTRA",
            "formName": "职工人事档案转出审批",
            "formNo": "workertranApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "o-WeabNuRtKw7UFscHfw-Q",
            "operateBean": "workertraOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "yj2BXZ4QTbmgfHlgYNeeyA",
        "moduleName": "职工人事档案材料转递审批",
        "moduleNo": "workertranApply",
        "moduleTypeId": "notice"
    },
    {
        "appId": "2bWkC0i8TgC6ZAWQlkHhLQ",
        "formId": {
            "dbTableName": "TD_CAR_USERINFO",
            "formName": "公务用车申请",
            "formNo": "carApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "w-W2bsu0QXKr9j7rEWJK4w",
            "operateBean": "CarApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.18.0.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "dZWW46RoTb-N3myJTbUW0A",
        "moduleName": "公务用车申请",
        "moduleNo": "carApply",
        "moduleTypeId": "car"
    },
    {
        "appId": "z05NHduxTfa6wDLa1TKfeA",
        "formId": {
            "dbTableName": "TD_HRMGT_ABROAD_REGISTER",
            "formName": "人员因私出国境审批",
            "formNo": "hrmgtAbroad",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "vQJNmuNAT7G4hbWdE8Vb_Q",
            "operateBean": "HrmgtAbroadOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "gAs8WWhrQ367t8W3aOgA6w",
        "moduleName": "员工因私出国境审批",
        "moduleNo": "hrmgtAbroad",
        "moduleTypeId": "newsInterview"
    },
    {
        "formId": {
            "dbTableName": "DB",
            "formName": "子流程测试表单",
            "formNo": "testFormInSub",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "eWw5ESYhRvSULbjnbK6sZw",
            "operateBean": "EmptyFormOperator",
            "operateType": 2,
            "remoteUrl": "rmi://10.159.139.102:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "vmHUSMhdQM2FevPFuc0F4w",
        "moduleName": "测试事项2",
        "moduleNo": "testModule2",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "o1kmdj1QT4WYlqT3n-YhvQ",
        "formId": {
            "dbTableName": "TD_OA_OFFICE_RECEIVED",
            "formName": "收文登记",
            "formNo": "ReceiveRegister",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "HMoe_GbBRo2VBMLxw48TpA",
            "operateBean": "ReceiveOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.18.0.122:1022"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "kmUT_TYuQAW1hwhPo9BY-g",
        "moduleName": "公司收文",
        "moduleNo": "ReceiveRegister",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "8i5-b6fRTcGHrpRjB5AQKw",
        "formId": {
            "dbTableName": "TD_OA_NEWS_INTERVIEW",
            "formName": "新闻采访开发",
            "formNo": "NewsInterviewkf",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "25rxsUvST9eSxWpNsvoPFA",
            "operateBean": "NewsInterviewOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.123:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "07CBIMVlT4aVL8U4Xq2ckA",
        "moduleName": "新闻采访开发",
        "moduleNo": "NewsInterviewkf",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "Q1EHxNoBQZ2CSpQJpL5ztQ",
        "formId": {
            "dbTableName": "TD_MEET_APPLY",
            "formName": "会议申请单",
            "formNo": "meetApplyTab",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Kfzdt9DMSeysyV4tpdKbjQ",
            "operateBean": "MeetApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.18.0.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "jCkQ5MpOSn2PbvBehAU99A",
        "moduleName": "会议申请",
        "moduleNo": "MeetApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "34u0xOFVRFSAx_ZyrmlRGQ",
        "formId": {
            "dbTableName": "TD_ZHMGT_VISIT_APPLY",
            "formName": "参观接待",
            "formNo": "visitApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Z0PL8p6oTW6SxIACtimlWw",
            "operateBean": "VisitApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.159:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "-WHcEFMtTbeDt1wCpqFNPw",
        "moduleName": "参观接待",
        "moduleNo": "visitApply",
        "moduleTypeId": "notice"
    },
    {
        "appId": "8i5-b6fRTcGHrpRjB5AQKw",
        "formId": {
            "dbTableName": "TD_DZJW_BSZZ_APPLY",
            "formName": "标识制作申请",
            "formNo": "logoProduceApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "-00nv5T-RJ--3B_fRdYhgw",
            "operateBean": "LogoProduceOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.200:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "3kRZk88ITBGLb6bQyZ5PhA",
        "moduleName": "标识制作申请",
        "moduleNo": "logoProduceApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "TPEVMW9NTwu4tQBojKaC1w",
        "formId": {
            "dbTableName": "TD_OA_OFFICE_NOTICE",
            "formName": "通文公告审批",
            "formNo": "ShortnotiApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "mctdYw8qSgaZ2sGbCnHicg",
            "operateBean": "ShortnoticeOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.121:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "ctuG5VxzSr6lj3h_nQe5Dg",
        "moduleName": "通知公告审批",
        "moduleNo": "ShortnotiApply",
        "moduleTypeId": "notice"
    },
    {
        "appId": "o1kmdj1QT4WYlqT3n-YhvQ",
        "formId": {
            "dbTableName": "TD_OA_OFFICE_DISPATCH",
            "formName": "部门发文",
            "formNo": "DepartmentDispatch",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "_3z435fhRTq8FQZuMu6QQQ",
            "operateBean": "DispatchOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.121:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "cJC5QRtVQ3qiRQ1fpajMXw",
        "moduleName": "部门发文",
        "moduleNo": "DepartmentDispatch",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "znb2M7TSQsqbFWV8ALeA4w",
        "formId": {
            "dbTableName": "TD_TRADE_ZGDBTAGL_ZGDBTA",
            "formName": "职工代表提案",
            "formNo": "empProposal",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "0NxEbGdGTVC7QgFqs8aqsw",
            "operateBean": "EmpProposalOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "-6HJkBN5ReeFNkvQz4pC0Q",
        "moduleName": "职工代表提案",
        "moduleNo": "empProposal",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "qMKnOevAQDq5H8x1f-RvPQ",
        "formId": {
            "dbTableName": "TD_ARCHIVE_CONSULT",
            "formName": "档案查阅申请",
            "formNo": "archivesconsult",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "XXA0H1CeSzi7N2BZWqhQyA",
            "operateBean": "archivesconsultOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "TNo1Kp0FR7-HYedJxbiYKw",
        "moduleName": "档案查阅申请",
        "moduleNo": "archivesconsult",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "znb2M7TSQsqbFWV8ALeA4w",
        "formId": {
            "dbTableName": "TD_TRADE_HLHJY_APPLY",
            "formName": "合理化建议申请",
            "formNo": "rationalizationApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Mi6CVyEUSCC4mwlkede_yg",
            "operateBean": "RationalizationOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "7AuMSGY4QNKyxTeArK52uA",
        "moduleName": "合理化建议申请",
        "moduleNo": "rationalizationApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "QHX8tTgYQeeLLLRw-NP06g",
        "formId": {
            "dbTableName": "TD_ARCHIVE_WORKERARC_NO",
            "formName": "非人事档案查借阅",
            "formNo": "workerarcNoApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "HkYICe6qThKOx3kFVvr6SA",
            "operateBean": "workerarcNoOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "azIFLmyTT8C2_4AmAKN3VA",
        "moduleName": "非人事档案查借阅",
        "moduleNo": "workerarcNoApply",
        "moduleTypeId": "notice"
    },
    {
        "formId": {
            "dbTableName": "111",
            "formName": "技术服务合同修订",
            "formNo": "contract/updateTechnicalContract",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "yGf-JAigRTOJbvvG4110aw",
            "operateBean": "UpdateContractService",
            "operateType": 2,
            "remoteUrl": "rmi://10.254.9.163:1024"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "f01b8ilfRSqmv_g1CrfBlg",
        "moduleName": "技术服务合同修订",
        "moduleNo": "updateContract",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "QHX8tTgYQeeLLLRw-NP06g",
        "formId": {
            "dbTableName": "TD_ARCHIVE_WORKERARC",
            "formName": "人事档案查借阅",
            "formNo": "workerarcApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "m-BSdEbpQpKRf71NCtfTBw",
            "operateBean": "workerarcOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "qZipWwTeR5mwL0w94KVLvQ",
        "moduleName": "人事档案查借阅",
        "moduleNo": "workerarcApply",
        "moduleTypeId": "notice"
    },
    {
        "formId": {
            "dbTableName": "TD_FINANCE_ADVANCE_RETURN",
            "formName": "履约保证金退还",
            "formNo": "advanceReturn",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Pa63MJ80TR2cs8i6PwUqwg",
            "operateBean": "AdvanceReturnOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.196:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "fESCwMJsRXSrUAWMo5MP-A",
        "moduleName": "履约保证金退还",
        "moduleNo": "advanceReturn",
        "moduleTypeId": "notice"
    },
    {
        "formId": {
            "dbTableName": "TD_ENGINEERING_ENGMATAPPLY",
            "formName": "工程材料价格签证审批",
            "formNo": "Engmatapply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "bUCcl-odRNeIAiqmBmj6yQ",
            "operateBean": "EngmatapplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.159:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "Iw3xbDFHSHWQJ6BgsnTdkQ",
        "moduleName": "工程材料价格签证审批流程",
        "moduleNo": "Engmatapply",
        "moduleTypeId": "notice"
    },
    {
        "appId": "1KKyQceHT6uFCdF462Ltiw",
        "formId": {
            "dbTableName": "TD_SECURITY_CONSULTLAW_INFO",
            "formName": "法律意见咨询提请流程申请1",
            "formNo": "consultlawApproval1",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "GpAlUt5dSxiPmpjcpFKOmA",
            "operateBean": "consultlawApprovalOperate",
            "operateType": 2
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "QdHnlz65Rr6m5lAr273XeQ",
        "moduleName": "法律意见咨询提请流程1申请",
        "moduleNo": "consultlawApproval1",
        "moduleTypeId": "newsInterview",
        "unionCode": "FVYJ"
    },
    {
        "formId": {
            "dbTableName": "TD_ASSETS_LOW_MAINTAIN",
            "formName": "低值易耗品维修申请",
            "formNo": "assetsLowMaintainApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "FxsOvbq2SEaDDY33gugwjQ",
            "operateBean": "AssetsLowMaintainOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "ezuuC8hbRUO0xJNQv_d_jg",
        "moduleName": "低值易耗品维修申请",
        "moduleNo": "assetsLowMaintainApply",
        "moduleTypeId": "notice",
        "unionCode": "assetsLowMaintainApply"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_TWGZ_NDHQTZZ",
            "formName": "年度红旗团组织",
            "formNo": "redCorps",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "RhJ8gUasRwawSx-AgTN7ww",
            "operateBean": "RedCorpsOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "rlzrefrZSTmJ-F33hG46ug",
        "moduleName": "年度红旗团组织",
        "moduleNo": "redCorps",
        "moduleTypeId": "notice",
        "unionCode": "redCorps"
    },
    {
        "formId": {
            "dbTableName": "00",
            "formName": "项目申报表单",
            "formNo": "CreateProjectApplication",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "cmmRWiFNSviZI2IlE5s93g",
            "operateBean": "ProjectApplicationService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "7A6KmIYxSvWx2VoU2gXZ9Q",
        "moduleName": "项目申报",
        "moduleNo": "CreateProjectApplication"
    },
    {
        "appId": "kxH6l9bvRcaIwCtpaahICA",
        "formId": {
            "dbTableName": "TD_ASSETS_ITBACK",
            "formName": "资产回退",
            "formNo": "itAssetBackApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "4DloNcv1S1euoFsBIaNVbg",
            "operateBean": "AssetBackOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "KDJX2TLnTqa7eA2buGrbfA",
        "moduleName": "资产退回申请",
        "moduleNo": "assetBackApply",
        "moduleTypeId": "notice",
        "unionCode": "assetBackApply"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_DZJW_XNJC_APPLY",
            "formName": "救济金申请",
            "formNo": "benefitApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "pKTTuaoQQiuGGR7zrvxfmg",
            "operateBean": "BenefitApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "qmTqiC46SxeNoyg4kyd3QA",
        "moduleName": "救济金申请",
        "moduleNo": "benefitApply",
        "moduleTypeId": "newsInterview",
        "unionCode": "benefitApply"
    },
    {
        "appId": "Fks1ZZm5RU6dxyeHpztBqQ",
        "formId": {
            "dbTableName": "TD_TRADE_HZCJH_APPLY",
            "formName": "互助储金会管理",
            "formNo": "hzcjh",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "rGOLoxYpR6-WYRpVjfCHuQ",
            "operateBean": "HzcjhOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "Mwcab9ZESS26rP2oqTpkfQ",
        "moduleName": "互助储金会管理",
        "moduleNo": "Hzcjh",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "Fks1ZZm5RU6dxyeHpztBqQ",
        "formId": {
            "dbTableName": "TD_HQFW_DAILY_REPAIR",
            "formName": "日常维修申报",
            "formNo": "dailyRepair",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "16_L_u79TICG-zfQIjWi4Q",
            "operateBean": "DailyRepairOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "6EI4mGAURNOi-tH3kLEriA",
        "moduleName": "日常维修申报审批",
        "moduleNo": "dailyRepair",
        "moduleTypeId": "notice"
    },
    {
        "appId": "34u0xOFVRFSAx_ZyrmlRGQ",
        "formId": {
            "dbTableName": "TD_ENGINEERING_REPBNSAPPLY_REQ",
            "formName": "零星维修项目需求审批",
            "formNo": "repbnsapplyDemand",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "xblP1D_GRL6qE3ZtyBFBkQ",
            "operateBean": "RepbnsapplyReqOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.159:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "JbXh2pHOQ968b1_CAcQfaA",
        "moduleName": "零星维修项目需求审批",
        "moduleNo": "repbnsapplyDemand",
        "moduleTypeId": "notice"
    },
    {
        "appId": "Fks1ZZm5RU6dxyeHpztBqQ",
        "formId": {
            "dbTableName": "TD_TRADE_WWXX_APPLY",
            "formName": "慰问金申请",
            "formNo": "wwjApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "qn1ag4wcTIitQiv-SLcu6A",
            "operateBean": "WwjApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.200:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "ROMbb-NRRPC0SHJMBBlxXA",
        "moduleName": "慰问金管理",
        "moduleNo": "wwjmgt",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_HELP_APPLY",
            "formName": "互助申请",
            "formNo": "helpApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "DnySpXTqTjyM3Ht1-Xq7lg",
            "operateBean": "HelpApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.122:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "yDA2QszoRJ2PhcMy_D9Z8w",
        "moduleName": "互助申请",
        "moduleNo": "helpApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "1KKyQceHT6uFCdF462Ltiw",
        "formId": {
            "dbTableName": "TD_SECURITY_PASSINANDOUT_INFO",
            "formName": "相关方人员出入证制证审批表",
            "formNo": "passinandoutInfo",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Tx42wZ1KQaafAf-iTsnPrg",
            "operateBean": "PassinandoutInfoOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "h1noeWHARsW68pQfs4xDTA",
        "moduleName": "相关方出入证制证审批",
        "moduleNo": "passinandoutInfo",
        "moduleTypeId": "notice",
        "unionCode": "XGFCR"
    },
    {
        "appId": "speJ6FPaQlGYmDBahKDUcw",
        "formId": {
            "dbTableName": "TD_HRMGT_GXZM_APPLY",
            "formName": "劳动关系和收入证明申请",
            "formNo": "gxzmForm",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "eS9Bleh2SveEHZrCwLdIiA",
            "operateBean": "gxzmFormOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "phs87Ps4QbGwokRHuAdOVg",
        "moduleName": "劳动关系和收入证明申请",
        "moduleNo": "gxzmForm",
        "moduleTypeId": "newsInterview",
        "unionCode": "LDFXHSRZMSQ"
    },
    {
        "formId": {
            "dbTableName": "td_assets_low_scrap",
            "formName": "低值易耗品报废申请",
            "formNo": "assetsLowScrapApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "YAGqW19HRLSF1OhRvbCDmQ",
            "operateBean": "AssetsLowScrapOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "_dSr5qhlThyo67eqf0bXSQ",
        "moduleName": "低值易耗品报废流程",
        "moduleNo": "assetsLowScrapApply",
        "moduleTypeId": "notice",
        "unionCode": "assetsLowScrapApply"
    },
    {
        "formId": {
            "dbTableName": "TD_ASSETS_LOW_CHANGE",
            "formName": "低值易耗品异动申请",
            "formNo": "assetsLowChangeApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "2oJ9ydM-TriJdUQRCp0VwA",
            "operateBean": "AssetsLowChangeOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.186:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "B-2EGd5ZQZ2SdCbvhLLUOg",
        "moduleName": "低值易耗品异动",
        "moduleNo": "assetsLowChangeApply",
        "moduleTypeId": "newsInterview",
        "unionCode": "assetsLowChangeApply"
    },
    {
        "formId": {
            "dbTableName": "TD_ASSET_FYWZCG_GKORG",
            "formName": "非烟物资归口部门申请",
            "formNo": "aFyGkTab",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "6mRtTHnwTHGcpG6wR-iylw",
            "operateBean": "AssetsFywzGkOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.186:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "D3Qfv1AKSmaJowNGhFVlow",
        "moduleName": "非烟物资归口部门申请",
        "moduleNo": "assetFyGk",
        "moduleTypeId": "notice",
        "unionCode": "assetFyGk"
    },
    {
        "formId": {
            "dbTableName": "TD_ASSET_FYWZCG_APPLY",
            "formName": "非烟物资配送部门申请",
            "formNo": "aFyCGTab",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "LVzBc0MmRLaFn6-55x9QBg",
            "operateBean": "AssetsFywzCGOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.177:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "pAiOfZ9bQjWiCblQIvaCiw",
        "moduleName": "非烟物资配送部门申请",
        "moduleNo": "assetFyCG",
        "moduleTypeId": "notice",
        "unionCode": "assetFyCG"
    },
    {
        "appId": "speJ6FPaQlGYmDBahKDUcw",
        "formId": {
            "dbTableName": "TD_OA_TRAINING",
            "formName": "外部培训申请",
            "formNo": "externalTraining",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "FQGw3verQWeMP2B70Pa63g",
            "operateBean": "ExternalTrainingOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "zkvCl0xOTJGOeIgQkXqtRA",
        "moduleName": "外部培训申请",
        "moduleNo": "externalTraining",
        "moduleTypeId": "notice",
        "unionCode": "externalTraining"
    },
    {
        "formId": {
            "dbTableName": "TD_ASSETS_ITINTERNET_REFORM",
            "formName": "网络改造申请",
            "formNo": "internetApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "plLFDDPNQ3mv8PUO7FeEeQ",
            "operateBean": "InternetReformOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "B94haE_sRXi2tmVompvcHA",
        "moduleName": "网络改造申请",
        "moduleNo": "internetApply",
        "moduleTypeId": "notice"
    },
    {
        "formId": {
            "dbTableName": "CTQTC_PROJECT_CONTRACT_REVISE",
            "formName": "项目修改表单",
            "formNo": "CreateProjectContractRevise",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "fp-HmQldTbyli_Z3PjmQ-g",
            "operateBean": "ProjectContractReviseService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "TGaSdSRuQkeHqo8zsV6AYA",
        "moduleName": "项目修改",
        "moduleNo": "CreateProjectContractRevise",
        "moduleTypeId": "newsInterview"
    },
    {
        "formId": {
            "dbTableName": "TD_ENGINEERING_DESIGNAPPLY",
            "formName": "设计任务委托申请",
            "formNo": "designApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "SmpSmz9lSECVviInFVf3xw",
            "operateBean": "DesignApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.159:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "5Q2My8ErTLSkqNhHn4etlg",
        "moduleName": "设计任务委托申请",
        "moduleNo": "designApply",
        "moduleTypeId": "notice"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_JSFA_APPLY",
            "formName": "劳动竞赛方案申报",
            "formNo": "measureBudget",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "9oDNzvlZSzS4LuNbZwj7NQ",
            "operateBean": "MeasureBudgetOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.172:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "B2Tbxt4PRq2f2dDHeDMwwQ",
        "moduleName": "劳动竞赛方案申报",
        "moduleNo": "measureBudget",
        "moduleTypeId": "newsInterview",
        "unionCode": "measureBudget"
    },
    {
        "appId": "1KKyQceHT6uFCdF462Ltiw",
        "formId": {
            "dbTableName": "TD_SECURITY_CONSULTLAW_INFO",
            "formName": "法律意见咨询提请流程申请",
            "formNo": "consultlawApproval",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "4iTbrmAVQlC5iR9Nu5JCvg",
            "operateBean": "consultlawApprovalOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "e4hGix-7TlWSXzXBlmO-Ww",
        "moduleName": "法律意见咨询提请流程申请",
        "moduleNo": "consultlawApproval",
        "moduleTypeId": "newsInterview",
        "unionCode": "LVYJZX"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_SSBJ_APPLY",
            "formName": "劳动竞赛实施办结审批",
            "formNo": "implementSummary",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "_KcvcPUHQfaP9uFi-pDteQ",
            "operateBean": "ImplementSummaryOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.172:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "wNyS_Iu3Tl6FVyQZJO_yIA",
        "moduleName": "劳动竞赛实施办结审批",
        "moduleNo": "implementSummary",
        "moduleTypeId": "newsInterview",
        "unionCode": "implementSummary"
    },
    {
        "formId": {
            "dbTableName": "CTQTC_PROJECT_ACCEPTANCE",
            "formName": "项目验收或鉴定表单",
            "formNo": "CreateProjectAcceptance",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "RexRlVHzSeSfnf8Kk1NApw",
            "operateBean": "ProjectAcceptanceService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "pH6UxhHUQcKL6G1HPOvteA",
        "moduleName": "项目验收和鉴定",
        "moduleNo": "CreateProjectAcceptance",
        "moduleTypeId": "newsInterview",
        "unionCode": "CreateProjectAcceptance"
    },
    {
        "formId": {
            "dbTableName": "TD_INTEGRATEDT_DXJLXM_APPLY",
            "formName": "大型奖励项目申请",
            "formNo": "integratedtDxjlApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "i5KWd0k_S_qaRHCZu6Wamw",
            "operateBean": "IntegratedtDxjlBeanOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "1y4fHd9bR62revGpRohieg",
        "moduleName": "大型奖励项目申请",
        "moduleNo": "integratedtDxjlApply",
        "moduleTypeId": "notice",
        "unionCode": "integratedtDxjlApply"
    },
    {
        "formId": {
            "dbTableName": "TD_ASSET_FXWZCG_ORG",
            "formName": "非烟物资部门采购申请",
            "formNo": "assetsFYWZCGOrgApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "lEsrlU_-SDmxIQUpiAqP4g",
            "operateBean": "AssetsFywzcgOrgOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.186:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "bckOJVXGQEKO8BmaX3QiyA",
        "moduleName": "非烟物资部门采购申请",
        "moduleNo": "assetsFYWZCGOrgApply",
        "moduleTypeId": "newsInterview",
        "unionCode": "assetsFYWZCGOrgApply"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_HLHJY_HANDLE",
            "formName": "合理化建议办结申报",
            "formNo": "RationalizeHandle",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "UXPqFTwwQdub40ASvvntxg",
            "operateBean": "RationalizeHandleOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "9Wb66B_1Q0q8CdzCz_AWiw",
        "moduleName": "合理化建议办结申报",
        "moduleNo": "rationalizeHandle",
        "moduleTypeId": "newsInterview",
        "unionCode": "HLHJYBJ"
    }
]

export default datas;