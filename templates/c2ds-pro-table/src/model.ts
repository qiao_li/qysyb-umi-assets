import * as entityService from './service';
import C2Page from './interfaces/C2Page';
import ENTITY from "./ENTITY";

export default {
    namespace: 'BLOCK_NAME',
    state: {
        contents: [],
        total: null,
        pageIndex: 1,
        pageSize: 10,
        selectedRowKeys: [],
        entityModalType: 'add',
        entityModalVisiable: false
    },
    reducers: {
        save(state, { payload: { contents, total, pageIndex, pageSize } }) {
            return { ...state, contents, total, pageIndex, pageSize };
        },
        selectedRowKeys(state, { payload: { selectedRowKeys } }) {
            return { ...state, selectedRowKeys }
        },
        openModal(state, { payload: { entityModalType } }) {
            return { ...state, entityModalType, ...{ entityModalVisiable: true } }
        },
        closeModal(state, ) {
            return { ...state, ...{ entityModalVisiable: false } }
        },
    },
    effects: {
        * fetch({ payload: { pageIndex = 1, pageSize, condition } }, { call, put }) {
            const { contents, total }: C2Page<ENTITY> = yield call(entityService.fetch, { pageIndex, pageSize, condition });
            yield put({
                type: 'save',
                payload: {
                    contents,
                    total,
                    pageIndex,
                    pageSize,
                },
            });
        },
        * delete({ payload: { id } }, { call, put, select }) {
            debugger
            yield call(entityService.remove, id);
            const pageIndex = yield select(state => state['BLOCK_NAME'].pageIndex);
            const pageSize = yield select(state => state['BLOCK_NAME'].pageSize);
            yield put({ type: 'fetch', payload: { pageIndex, pageSize } });
        },
        * patch({ payload: { id, values } }, { call, put, select }) {
            yield call(entityService.patch, id, values);
            const page = yield select(state => state['BLOCK_NAME'].page);
            yield put({ type: 'fetch', payload: { page } });
        },
        * create({ payload: values }, { call, put, select }) {
            let result = yield call(entityService.create, values);
            const page = yield select(state => state['BLOCK_NAME'].page);
            yield put({ type: 'fetch', payload: { page } });
            if (result === 'ok' || result === '200') {
                return 'ok';
            } else {
                return 'error';
            }
        },
        * update({ payload: { id, values } }, { call, put, select }) {
            let result = yield call(entityService.update, { id, values });
            const pageSize = yield select(state => state['BLOCK_NAME'].pageSize);
            const pageIndex = yield select(state => state['BLOCK_NAME'].pageIndex);
            yield put({ type: 'fetch', payload: { pageSize, pageIndex } });
            if (result === 'ok' || result === '200') {
                return 'ok';
            } else {
                return 'error';
            }
        },
    },
};
