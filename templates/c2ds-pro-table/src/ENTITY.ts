import { Column, Entity, Index } from "typeorm";

@Index("UNION_MODULE", ["unionCode"], {})
@Index("FK_QYBWF_MO_REFERENCE_QYBWF_FO", ["formId"], {})
@Entity("qybwf_module", { schema: "withyact" })
export default class QybwfModule {
    @Column("varchar", { primary: true, name: "ID", length: 50 })
    id: string;

    @Column("varchar", { name: "MODULE_NO", nullable: true, length: 50 })
    moduleNo: string | null;

    @Column("varchar", { name: "MODULE_NAME", nullable: true, length: 100 })
    moduleName: string | null;

    @Column("varchar", { name: "MODULE_DESCRIBE", nullable: true, length: 500 })
    moduleDescribe: string | null;

    @Column("varchar", { name: "MODULE_TYPE_ID", nullable: true, length: 50 })
    moduleTypeId: string | null;

    @Column("varchar", { name: "GROUP_ID", nullable: true, length: 50 })
    groupId: string | null;

    @Column("varchar", { name: "PROCESS_DEF_ID", nullable: true, length: 50 })
    processDefId: string | null;

    @Column("varchar", { name: "PROCESS_START_URL", nullable: true, length: 200 })
    processStartUrl: string | null;

    @Column("varchar", { name: "FORM_ID", nullable: true, length: 50 })
    formId: string | null;

    @Column("varchar", { name: "FORM_TYPE", nullable: true, length: 50 })
    formType: string | null;

    @Column("varchar", { name: "MOBILE_FORM_ID", nullable: true, length: 255 })
    mobileFormId: string | null;

    @Column("varchar", { name: "ICO", nullable: true, length: 200 })
    ico: string | null;

    @Column("varchar", { name: "UNION_CODE", nullable: true, length: 255 })
    unionCode: string | null;

    @Column("varchar", { name: "ORG_ID", nullable: true, length: 100 })
    orgId: string | null;

    @Column("varchar", { name: "ORG_NO", nullable: true, length: 100 })
    orgNo: string | null;

    @Column("varchar", { name: "ORG_NAME", nullable: true, length: 100 })
    orgName: string | null;

    @Column("varchar", { name: "ORG_LEVEL", nullable: true, length: 100 })
    orgLevel: string | null;

    @Column("varchar", { name: "ADMIN_ID", nullable: true, length: 100 })
    adminId: string | null;

    @Column("varchar", { name: "ADMIN_NAME", nullable: true, length: 100 })
    adminName: string | null;

    @Column("varchar", { name: "ADMIN_PHONE", nullable: true, length: 100 })
    adminPhone: string | null;

    @Column("varchar", { name: "TENANT_ID", nullable: true, length: 100 })
    tenantId: string | null;

    @Column("varchar", { name: "APP_ID", nullable: true, length: 100 })
    appId: string | null;

    form: string;
}
