const datas = [
    {
        "appId": "speJ6FPaQlGYmDBahKDUcw",
        "formId": {
            "dbTableName": "TD_OA_TRAINING_EDUCATION",
            "formName": "自费教育",
            "formNo": "educationApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "4wHIQ1OJRAO7tPxidO_Avg",
            "operateBean": "EducationApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "Qo8RYnjUQy2tRVMPIobnaw",
        "moduleName": "自费教育",
        "moduleNo": "education",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "kxH6l9bvRcaIwCtpaahICA",
        "formId": {
            "dbTableName": "TD_ASSETS_ITMAINTAIN",
            "formName": "资产维修申请",
            "formNo": "assetMaintainAdd",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "CmK0-qsoSQuxEPLA0OEd1w",
            "operateBean": "AssetMaintainOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "O7WgrjsDR86MqH4ZMZVDYg",
        "moduleName": "资产维修申请",
        "moduleNo": "assetMaintainAdd",
        "moduleTypeId": "notice"
    },
    {
        "appId": "Y6GVieevScWw-k2MzjB3Vg",
        "formId": {
            "dbTableName": "111",
            "formName": "论文投稿申报",
            "formNo": "lwtgsb",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "Fy3p94wcSfepOZ3f4mLmvQ",
            "operateBean": "PaperInfoService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.155:1022"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "MSn47pTeQa-kdY7XRDREMQ",
        "moduleName": "论文投稿申报",
        "moduleNo": "lwtgsb",
        "moduleTypeId": "newsInterview"
    },
    {
        "formId": {
            "dbTableName": "fgfhjkl",
            "formName": "年度报告",
            "formNo": "CreateAnnualReport",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "5Cd-NDL1S2a7MXtmhSQ0Dg",
            "operateBean": "AnnualReportService",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.170:1023"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "KJwoT4nOTaaPLIoXUjcL_g",
        "moduleName": "年度报告",
        "moduleNo": "CreateAnnualReport",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_JSJH_APPLY",
            "formName": "劳动竞赛计划申报",
            "formNo": "competitionPlan",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "1TEVZTrCT0mSPQyqcdWE_w",
            "operateBean": "CompetitionPlanOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "PtUY_CEBTha-K2nniVdgEg",
        "moduleName": "劳动竞赛计划申报",
        "moduleNo": "competitionPlan",
        "moduleTypeId": "newsInterview"
    },
    {
        "appId": "1KKyQceHT6uFCdF462Ltiw",
        "formId": {
            "dbTableName": "TD_SECURITY_PASSCAR",
            "formName": "相关车辆出入证制证审批表",
            "formNo": "passcar",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "UvTd8n3LSEK2RpXNDXbn3g",
            "operateBean": "PasscarOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.173:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "nd_dzXh1Q0ejfBVnMFv6BA",
        "moduleName": "相关车辆出入证制证",
        "moduleNo": "passcar",
        "moduleTypeId": "notice",
        "unionCode": "XGCLCR"
    },
    {
        "appId": "Fks1ZZm5RU6dxyeHpztBqQ",
        "formId": {
            "dbTableName": "TD_DZJW_DHGL_APPLY",
            "formName": "党徽申领管理",
            "formNo": "dhglApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "lTIxeUqnSzGpljfOxr-bbQ",
            "operateBean": "DhglApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.200:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "lvgZEdRaRM-VTVVcRsditA",
        "moduleName": "党徽申领管理",
        "moduleNo": "dhglApply",
        "moduleTypeId": "newsInterview",
        "unionCode": "dhglApply"
    },
    {
        "appId": "3l-cOKtSQ62IFzhrNdYuuA",
        "formId": {
            "dbTableName": "TD_TRADE_TWGZ_XFJ",
            "formName": "青春先锋奖申请",
            "formNo": "youthAward",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "nWAYzQBfR_q2_bwngKJsLQ",
            "operateBean": "YouthAwardOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "AgjrKjE2Qaut1KAyoWdcEw",
        "moduleName": "青春先锋奖申请",
        "moduleNo": "youthAward",
        "moduleTypeId": "notice",
        "unionCode": "youthAward"
    },
    {
        "appId": "speJ6FPaQlGYmDBahKDUcw",
        "formId": {
            "dbTableName": "TD_OA_TRAINING",
            "formName": "内部培训申请",
            "formNo": "internalTraining",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "QDKsB9BLRPC_HY8adJOtLA",
            "operateBean": "InternalTrainingOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.95:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "irS1sIZlSTG4QYPhvRiCEg",
        "moduleName": "内部培训",
        "moduleNo": "internalTraining",
        "moduleTypeId": "notice"
    },
    {
        "appId": "kxH6l9bvRcaIwCtpaahICA",
        "formId": {
            "dbTableName": "TD_ASSETS_ITCOMMUNICATION",
            "formName": "通讯流程申请",
            "formNo": "communicationApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "FLG5ktOQTYO4V69WrODPSQ",
            "operateBean": "CommunicationOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "PK8h7eraRlevEqNi_3nrDA",
        "moduleName": "通讯维护申请",
        "moduleNo": "communicationApply",
        "moduleTypeId": "notice"
    },
    {
        "appId": "kxH6l9bvRcaIwCtpaahICA",
        "formId": {
            "dbTableName": "TD_ASSETS_ITRECEIVE_APPLY",
            "formName": "IT资产领用申请",
            "formNo": "assetRApply",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "N1eRum5lRMCUgsrN7Au-PQ",
            "operateBean": "AssetRApplyOperate",
            "operateType": 2,
            "remoteUrl": "rmi://172.16.81.199:1021"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "y0SfGTDUREizauMijY2IoQ",
        "moduleName": "IT资产领用申请",
        "moduleNo": "assetRApply",
        "moduleTypeId": "newsInterview"
    },
    {
        "formId": {
            "dbTableName": "111",
            "formName": "技术服务合同登记",
            "formNo": "contract/createTechnicalContract",
            "frontEndEngine": "c2_form",
            "hasDbTable": true,
            "id": "_f0AJB-IRpSE4SSelIqHNw",
            "operateBean": "TechnicalContractService",
            "operateType": 2,
            "remoteUrl": "rmi://10.254.9.163:1024"
        },
        "ico": "ext/moduleIcons/app-systempref.png",
        "id": "ZgV4Dh7YTQ26kmgJwPbgBA",
        "moduleName": "技术服务合同登记",
        "moduleNo": "technicalContract",
        "moduleTypeId": "newsInterview"
    }
]

export default datas;