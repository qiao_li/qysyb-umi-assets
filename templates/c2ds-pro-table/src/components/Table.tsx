import { Button, Divider, Dropdown, Icon, Menu, Popconfirm } from 'antd';
import React from 'react';
import ProTable, { ProColumns, RequestData } from '@ant-design/pro-table';
import UmiUIFlag from '@umijs/ui-flag';
import { connect } from 'dva';
import ENTITY from "../ENTITY";
import EntityEditModal from "./EntityEditModal";

class Table extends React.Component<{
    dispatch,
    contents,
    loading,
    total,
    pageIndex,
    selectedRowKeys,
    entityModalType,
    entityModalVisiable
}, {
    editEntityId
}> {
    // const [sorter, setSorter] = useState({});
    ref
    constructor(props) {
        super(props)
        this.ref = React.createRef();
        this.state = {
            editEntityId: ''
        }
    }
    pageSize: number
    rowKey: string = 'id';
    columns: ProColumns<ENTITY>[] = [
        {
            title: '事项编码',
            dataIndex: 'moduleNo',
        },
        {
            title: '事项名称',
            dataIndex: 'moduleName',
        },
        {
            title: '表单名称',
            dataIndex: 'formId.formName',
            sorter: true,
            align: 'right',
            renderText: (val: string) => `[ ${val} 自定义]`,
        },
        {
            title: '状态',
            dataIndex: 'status',
            valueEnum: {
                0: { text: '关闭', status: 'Default' },
                1: { text: '运行中', status: 'Processing' },
                2: { text: '已上线', status: 'Success' },
                3: { text: '异常', status: 'Error' },
            },
        },
        {
            title: '操作',
            dataIndex: 'option',
            valueType: 'option',
            render: (text, record, index, actionRef) => (
                <>
                    <a onClick={() => { this.recordViewClick(text, record, index, actionRef) }}>查看</a>
                    <Divider type="vertical" />
                    <a onClick={() => { this.recordEditClick(text, record, index, actionRef) }}>编辑</a>
                    <Divider type="vertical" />
                    <Popconfirm
                        placement="left"
                        title="确定删除?"
                        onConfirm={() => { this.recordDeleteClick(text, record, index, actionRef) }}
                        okText="确认"
                        cancelText="取消"
                    >
                        <a>删除</a>
                    </Popconfirm>
                </>
            ),
        },
    ];

    // {
    //     title: '操作',
    //     dataIndex: 'option',
    //     valueType: 'option',
    //     render: (text, record, index, actionRef) => (
    //         <>
    //             <a onClick={() => { this.recordViewClick(text, record, index, actionRef) }}>查看</a>
    //             <Divider type="vertical" />
    //             <a onClick={() => { this.recordEditClick(text, record, index, actionRef) }}>编辑</a>
    //             <Divider type="vertical" />
    //             <Popconfirm
    //                 title="确定删除?"
    //                 onConfirm={() => { this.recordDeleteClick(text, record, index, actionRef) }}
    //                 okText="确认"
    //                 cancelText="取消"
    //             >
    //                 <a>删除</a>
    //             </Popconfirm>
    //         </>
    //     ),
    // },

    // 操作删除点击
    recordDeleteClick(text, record, index, actionRef) {
        this.props.dispatch({
            type: 'BLOCK_NAME/delete',
            payload: {
                id: record[this.rowKey]
            }
        });
    }

    // 操作查看点击
    recordViewClick(text, record, index, actionRef) {
        this.props.dispatch({
            type: 'BLOCK_NAME/openModal',
            payload: {
                entityModalType: 'view'
            }
        });
        // 这个数据没有通过 dva 机制。而是用 state
        this.setState({
            editEntityId: record[this.rowKey]
        })
    }

    // 操作编辑点击
    recordEditClick(text, record, index, actionRef) {
        this.props.dispatch({
            type: 'BLOCK_NAME/openModal',
            payload: {
                entityModalType: 'edit'
            }
        });
        this.setState({
            editEntityId: record[this.rowKey]
        })
    }

    // 初始化数据加载
    componentDidMount() {
        this.pageSize = 10;
        this.props.dispatch({
            type: 'BLOCK_NAME/fetch',
            payload: { pageIndex: 1, pageSize: 10 }
        });
    }

    // 分页改变
    onChange = (pageIndex, pageSize) => {
        this.pageSize = pageSize;
        this.props.dispatch({
            type: 'BLOCK_NAME/fetch',
            payload: { pageIndex, pageSize }
        });
    }
    // pageSize 改变
    onShowSizeChange = (pageIndex, pageSize) => {
        this.pageSize = pageSize;
        this.props.dispatch({
            type: 'BLOCK_NAME/fetch',
            payload: { pageIndex, pageSize }
        });
    }

    // 选择列改变
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.props.dispatch({
            type: "BLOCK_NAME/selectedRowKeys",
            payload: { selectedRowKeys }
        })
    };

    // 新增按钮
    onCreate = () => {
        this.props.dispatch({
            type: 'BLOCK_NAME/openModal',
            payload: {
                entityModalType: 'add'
            }
        })
    }

    onSearchSubmit = (params) => {
        let realCond = this.getConditionObject(params);
        this.props.dispatch({
            type: 'BLOCK_NAME/fetch',
            payload: {
                condition: realCond,
                pageIndex: this.props.pageIndex,
                pageSize: this.pageSize,
            }
        })
    }

    // C2高级查询拼装
    getConditionObject = (params) => {
        let filters = {};
        let rules = [];
        for (let p in params) {
            let rule = {
                field: p,
                op: "cn", //like
                data: params[p]
            }
            rules.push(rule);
            // { "field": "moduleNo", "op": "cn", "data": "11" }
        };
        filters = {
            groupOp: "AND",
            rules
        };
        return {
            filters
        }
    }

    render() {
        return (
            <ProTable<ENTITY>
                headerTitle="查询表格"
                actionRef={this.ref}
                rowKey={this.rowKey}
                loading={this.props.loading}
                scroll={{ x: true }}
                rowSelection={{
                    selectedRowKeys: this.props.selectedRowKeys,
                    onChange: this.onSelectChange,
                }}
                // onChange={(_, _filter, _sorter) => {
                //   setSorter(`${_sorter.field}_${_sorter.order}`);
                // }}
                // params={{
                //     sorter,
                // }}
                toolBarRender={(action, { }) => [
                    <>
                        <UmiUIFlag />
                    </>,
                    <>
                        <EntityEditModal
                            type={this.props.entityModalType}
                            visible={this.props.entityModalVisiable}
                            dispatch={this.props.dispatch}
                            id={this.state.editEntityId} />
                        <Button icon="plus" type="primary" onClick={this.onCreate}>新建</Button>
                    </>,
                    this.props.selectedRowKeys && this.props.selectedRowKeys.length > 0 && (
                        <Dropdown
                            overlay={
                                <Menu
                                    onClick={async e => {
                                        if (e.key === 'remove') {
                                            console.log('remove');
                                            action.reload();
                                        }
                                    }}
                                    selectedKeys={[]}
                                >
                                    <Menu.Item key="remove">批量删除</Menu.Item>
                                    <Menu.Item key="approval">批量审批</Menu.Item>
                                </Menu>
                            }
                        >
                            <Button>
                                批量操作 <Icon type="down" />
                            </Button>
                        </Dropdown>
                    ),
                ]}
                // tableAlertRender={(selectedRowKeys, selectedRows) => (
                //   <div>
                //     已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
                //     <span>
                //       服务调用次数总计 {selectedRows.reduce((pre, item) => pre + item.callNo, 0)} 万
                //     </span>
                //   </div>
                // )}
                // request={params => this.queryDatas(params, this.props.dispatch)}
                dataSource={this.props.contents}
                pagination={{
                    onChange: this.onChange,
                    total: this.props.total,
                    showSizeChanger: true,
                    defaultPageSize: 10,
                    onShowSizeChange: this.onShowSizeChange
                }}
                columns={this.columns}
                options={{
                    density: true,
                    fullScreen: true,
                    // reload: (a, b) => {
                    //     debugger
                    //     console.log(1)
                    // },
                    reload: false,
                    setting: true
                }}
                onSubmit={(params) => {
                    this.onSearchSubmit(params)
                }}
            />
        )
    }
};
// redux 与 dva 知识
function mapStateToProps(state) {
    const {
        contents,
        total,
        pageIndex,
        selectedRowKeys,
        entityModalType,
        entityModalVisiable
    } = state['BLOCK_NAME'];
    return {
        contents,
        total,
        pageIndex,
        loading: state.loading.models['BLOCK_NAME'],
        selectedRowKeys,
        entityModalType,
        entityModalVisiable
    };
}

export default connect(mapStateToProps)(Table);