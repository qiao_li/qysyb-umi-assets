import React, { Component } from 'react'
import { WrappedDynamicForm as DynamicForm, FormScheme } from "qyb-react-easyui";
import { Modal, Button } from "antd";
import { get, update } from "../service";

export default class EntityEdit extends Component<{
    type: 'edit' | 'add' | 'view',
    id: string,
    dispatch: any,
    visible: boolean,
}, {
    title: string,
    formScheme: any,
    loading: boolean
}> {

    titleMap = {
        edit: '编辑',
        add: '新增',
        view: '查看'
    }

    constructor(props) {
        super(props)
        this.state = {
            title: this.titleMap[props.type],
            formScheme: this.getformScheme(),
            loading: false
        };
    }

    disabled: boolean;
    formValues: any;
    propForm: any;

    getformScheme() {
        let formScheme = {
            key: "entityEdit",
            name: "实体编辑表单",
            props: {},
            children: [
                {
                    key: "id",
                    name: "id",
                    props: {
                        hidden: true
                    },
                    class: {
                        componentTag: "AntInput"
                    }
                },
                {
                    key: "moduleNo",
                    name: "事项编码",
                    props: {
                        span: 12,
                        required: true,
                        label: "事项编码",
                        disabled: this.disabled
                    },
                    class: {
                        componentTag: "AntInput"
                    }
                },
                {
                    key: "moduleName",
                    name: "事项名称",
                    props: {
                        span: 12,
                        required: true,
                        label: "事项名称",
                        disabled: this.disabled
                    },
                    class: {
                        componentTag: "AntInput"
                    }
                },
                {
                    key: 'formId',
                    name: '表单嵌套对象',
                    children: [
                        {
                            key: "formName",
                            name: "表单名称",
                            props: {
                                span: 12,
                                required: true,
                                label: '表单嵌套对象',
                                disabled: this.disabled
                            },
                            class: {
                                componentTag: "AntInput"
                            }
                        },
                    ]
                }
            ],
            onValuesChange: (changed, allValues) => {
                this.formValues = allValues;
            }
        }
        return formScheme;
    }

    // 确认
    handleOk = async (e) => {
        e.preventDefault();
        let values = this.propForm.getFieldsValue()
        this.propForm.validateFieldsAndScroll(async (err, values) => {
            if (!err) {
                let result;
                if (this.props.type === 'add') {
                    result = await this.props.dispatch({
                        type: 'BLOCK_NAME/create',
                        payload: values
                    })
                } else if (this.props.type === 'edit') {

                    result = await this.props.dispatch({
                        type: 'BLOCK_NAME/update',
                        payload: { id: this.props.id, values }
                    })
                }

                if (result === 'ok') {
                    await this.props.dispatch({
                        type: 'BLOCK_NAME/closeModal'
                    })
                    // 清空表单值，防止下一次打开modal还有值显示，也可以再每次Modal显示时 reset 不同的思路
                    setTimeout(() => {
                        this.propForm.resetFields();
                    }, 200)
                } else {

                }
            }
        });

    }

    handleCancel = () => {
        this.props.dispatch({
            type: 'BLOCK_NAME/closeModal'
        });
        setTimeout(() => {
            this.propForm.resetFields();
        }, 200);
    }

    componentDidUpdate = (prevProps) => {
        // 编辑查看打开才去获取
        if (this.props.visible !== prevProps.visible
            && this.props.visible
            && (this.props.type === 'edit' || this.props.type === 'view')
            && this.props.id) {
            if (this.props.type === 'view') {
                this.propForm
            }
            get(this.props.id).then(data => {
                this.propForm.setFieldsValue(data);
            })
        }
        if (this.props.type !== prevProps.type) {
            this.disabled = this.props.type === 'view' ? true : false;
            // trigger update
            this.setState({
                title: this.titleMap[this.props.type],
                formScheme: this.getformScheme()
            });
        }
    }

    render() {
        return (
            <Modal
                title={this.state.title}
                visible={this.props.visible}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="back" onClick={this.handleCancel}>
                        取消
                    </Button>,
                    <Button key="submit" type="primary" loading={this.state.loading} onClick={this.handleOk}>
                        确认
                    </Button>,
                ]}>
                <DynamicForm formScheme={this.state.formScheme} wrappedComponentRef={(c) => {
                    if (c && c.props.form) {
                        this.propForm = c.props.form
                        console.log(this.propForm)
                    }
                }}></DynamicForm>
            </Modal>
        )
    }
}
