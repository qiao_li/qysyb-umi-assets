import request from 'umi-request';

const API_PREFIX = `/proxy/REST_PATH`;

export function fetch({ pageIndex, pageSize, condition }) {
    let requestUrl = `${API_PREFIX}?page=${pageIndex}&rows=${pageSize}`;
    if (condition) {
        requestUrl = requestUrl + '&cond=' + JSON.stringify(condition);
    }
    return request(requestUrl);
}

export function remove(id) {
    return request(`${API_PREFIX}/${id}`, {
        method: 'DELETE',
    });
}

export function patch(id, values) {
    // TODO:
    // use umi-request after the issue is closed
    // https://github.com/umijs/umi-request/issues/5
    return window.fetch(`${API_PREFIX}/${id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
    });
}

export function create(values) {
    return request(API_PREFIX, {
        method: 'POST',
        data: values,
    });
}

export function get(id) {
    return request(API_PREFIX + `/${id}`, {
        method: 'GET',
        params: {
            id
        }
    });
}

export function update({ id, values }) {
    return request(API_PREFIX + `/${id}`, {
        method: 'POST',
        data: values
    });
}